Tag: odd-mark-in-description
Severity: pedantic
Check: fields/description
Explanation: A punction mark was placed oddly in the description.
 .
 This tag is currently only issued for a comman that is
 followed by a non-whitespace character.
See-Also: Bug#591665, Bug#591664
