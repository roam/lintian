Tag: wrong-vcs-location-for-dpmt
Severity: error
See-Also: DPMT policy
Check: fields/vcs
Explanation: This package is maintained within the Debian Python Modules Team (DPMT)
 and as such, its VCS should live in Salsa under
 <code>https://salsa.debian.org/python-team/modules/</code>.
 .
 This is not currently the case and the package's VCS should be migrated to the
 proper location.
