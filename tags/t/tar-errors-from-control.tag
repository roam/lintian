Tag: tar-errors-from-control
Severity: error
Check: deb-format
Explanation: tar produced an error while listing the contents of the
 <code>control.tar.gz</code> member of this package. This probably means
 there's something broken or at least strange about the way the package
 was constructed.
