#!/usr/bin/perl
#
# Copyright © 2020 Felix Lechner
#
# This program is free software.  It is distributed under the terms of
# the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.

use v5.20;
use warnings;
use utf8;

use HTTP::Tiny;
use Path::Tiny;
use Time::Moment;

# for Lintian's data/standards-version/release-dates

my $POLICY_CHANGELOG_URL
  = 'https://salsa.debian.org/dbnpolicy/policy/-/raw/master/debian/changelog?inline=false';

my $tempfile = Path::Tiny->tempfile;
my $path = $tempfile->stringify;

my $response = HTTP::Tiny->new->get($POLICY_CHANGELOG_URL);
die "Failed!\n"
  unless $response->{success};

$tempfile->spew_utf8($response->{content});

my $rfc822 = qx{dpkg-parsechangelog --format rfc822 --all --file $path};
my %release_date;

# other parsers are more efficient
my @sections = split(/\n\n/, $rfc822);
for my $section (@sections) {

    my ($version) = ($section =~ /^Version:\s+(\S+)\s*$/m);
    my ($epoch) = ($section =~ /^Timestamp:\s+(\S+)\s*$/m);
    $release_date{$version} = $epoch;
}

# use epoch for chrononological order
for my $version (sort keys %release_date) {
    my $timestamp
      = Time::Moment->from_epoch($release_date{$version})->strftime('%c');
    say "Policy $version is from $timestamp";
}

# Local Variables:
# indent-tabs-mode: nil
# cperl-indent-level: 4
# End:
# vim: syntax=perl sw=4 sts=4 sr et
